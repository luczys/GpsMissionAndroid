package com.example.filipluczys.gpsmission;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener {

    private GoogleMap mMap;

    private Marker mTarget;
    private Marker mPosition;
    private double relativeCourse = 0.0;

    private final MyHandler mHandler = new MyHandler(this);
    private BluetoothDevice mDdevice;
    private BtConnection btConnection;

    private final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private final int REQUEST_ENABLE_BT = 2;
    final static int MESSAGE_READ = 3;
    final static int STATUS_CHANGE = 4;

    private static final String TAG = "GpsMission";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mDdevice = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        // Add a marker in Sydney and move the camera
        LatLng wrocLatLng = new LatLng(51.075783, 17.025674); //get current or add wroclaw
        mTarget = mMap.addMarker(new MarkerOptions().position(wrocLatLng).title("Target").draggable(true));
        mPosition = mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Position").draggable(false).visible(false).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(wrocLatLng));

        // Set a listener for marker click.
        mMap.setOnMarkerDragListener(this);

        setText();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    try {
                        mMap.setMyLocationEnabled(true);
                    } catch (SecurityException e) {
                        Toast toast = Toast.makeText(getApplicationContext(), "No location!", Toast.LENGTH_SHORT);
                        toast.show();
                    }
            }
        }
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        setText();
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        setText();
        sendTarget();
    }


    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT: {
                // If request is cancelled, the result arrays are empty.
                if (resultCode == RESULT_OK) {
                    setupBt();
                }
            }
        }
    }


    public void btButtonClick(View view) {
        setupBt();

    }

    public void setupBt() {
        if (mDdevice != null) {
            connectBt(mDdevice);
        } else {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter != null) {
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                } else {
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                            MainActivity.this,
                            android.R.layout.select_dialog_singlechoice);

                    final ArrayList<BluetoothDevice> devices = new ArrayList<>();

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

                    alertDialogBuilder
                            .setTitle("Click to connect!")
                            .setCancelable(false)
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //String strName = arrayAdapter.getItem(which);
                                    mDdevice = devices.get(which);
                                    dialog.cancel();
                                    connectBt(mDdevice);
                                }
                            })
                    ;

                    AlertDialog alertDialog = alertDialogBuilder.create();

                    alertDialog.show();

                    mBluetoothAdapter.startDiscovery();
                    // Create a BroadcastReceiver for ACTION_FOUND
                    final BroadcastReceiver mReceiver = new BroadcastReceiver() {
                        public void onReceive(Context context, Intent intent) {
                            String action = intent.getAction();
                            // When discovery finds a device
                            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                                // Get the BluetoothDevice object from the Intent
                                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                                devices.add(device);
                                // Add the name and address to an array adapter to show in a ListView
                                arrayAdapter.add(device.getName() + "\n" + device.getAddress());
                            }
                        }
                    };
                    //TODO: Make sure that paired avaialble device is shown
                    // Register the BroadcastReceiver
                    IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                    registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
                }
            }
        }
    }

    public void connectBt(BluetoothDevice device) {
        btConnection = new BtConnection(device, BluetoothAdapter.getDefaultAdapter(), mHandler);
        btConnection.start();

    }

    public void setText() {
        TextView textView;
        textView = (TextView) findViewById(R.id.textView);
        String connected;
        if (btConnection == null || !btConnection.connected) {
            connected = "Not Connected";
        } else {
            connected = "Connected";
        }

        String s = String.format(Locale.getDefault(), "Target: %.6f %.6f\nBT: %s\nPossition: %.6f %.6f\n" +
                "Relative course: %.2f", mTarget.getPosition().latitude, mTarget.getPosition().longitude, connected, mPosition.getPosition().latitude, mPosition.getPosition().longitude, relativeCourse);

        textView.setText(s);
    }

    private class Codec {
        boolean inBuff = false;
        boolean nextEscape = false;
        int buffOff = 0;
        byte[] buff = new byte[100];

        private static final byte startFlag = (byte) 0xFE;
        private static final byte escapeFlag = (byte) 0xFD;
        private static final byte escapeMod = (byte) 0xDF;
        private static final byte unescapeMod = (byte) 0x20;

        public static final byte targetHeader = (byte) 0x00;
        public static final byte positionHeader = (byte) 0x01;
        public static final byte relativeCourseHeader = (byte) 0x02;

         int encode(byte[] bytes, byte[] res) {
            int off = 0;
            res[off++] = startFlag;
            res[off++] = targetHeader;

            for (byte val: bytes) {
                off = encodeByte(val, res, off);
            }

             res[off++] = startFlag;
            return off;
        }

        int encodeByte(byte val, byte[] bytes, int off) {
            if (val != startFlag && val != escapeFlag) {
                bytes[off] = val;
                return off + 1;
            } else {
                bytes[off] = escapeFlag;
                bytes[off + 1] = (byte) (val & escapeMod);
                return off + 2;
            }
        }

        boolean decode(byte val) {
            if (!inBuff) {
                if (val == startFlag) {
                    inBuff = true;
                    buffOff = 0;
                }
            } else {
                if (val == startFlag)
                {
                    if  ( buffOff > 0 ){
                    inBuff = false;
                    nextEscape = false;
                    return true;
                }
                } else {
                    if (val != escapeFlag) {
                        buff[buffOff] = val;
                        if (nextEscape) {
                            buff[buffOff] |= unescapeMod;
                        }
                        buffOff++;
                        nextEscape = false;
                        if (buffOff > buff.length)
                        {
                            buffOff = 0;
                            inBuff = false;
                        }
                    } else {
                        nextEscape = true;
                    }
                }
            }
            return false;
        }

        byte decodeHeader()
        {
            return buff[0];
        }

        LatLng decodePosition() {
            int lat = (0xFF & buff[1]) | ((0xFF & buff[2]) << 8) | ((0xFF & buff[3]) << 16) | ((0xFF & buff[4]) << 24);
            int lon = (0xFF & buff[5]) | ((0xFF & buff[6]) << 8) | ((0xFF & buff[7]) << 16) | ((0xFF & buff[8]) << 24);

            return new LatLng((double) lat / 1000000, (double) lon / 1000000);
        }

        double decodeRelativeCourse() {
            int course = (0xFF & buff[1]) | ((0xFF & buff[2]) << 8) | ((0xFF & buff[3]) << 16) | ((0xFF & buff[4]) << 24);
            return course / 100.0;
        }

    }

    Codec mCodec = new Codec();

    public void sendTarget() {
        if (btConnection != null) {
            byte[] bytes = new byte[2 * 4];
            byte[] res = new byte[bytes.length * 2 + 2];
            int lat = (int) (mTarget.getPosition().latitude * 1000000);
            int lon = (int) (mTarget.getPosition().longitude * 1000000);

            bytes[0] = (byte) lat;
            bytes[1] = (byte) (lat >> 8);
            bytes[2] = (byte) (lat >> 16);
            bytes[3] = (byte) (lat >> 24);

            bytes[4] = (byte) lon;
            bytes[5] = (byte) (lon >> 8);
            bytes[6] = (byte) (lon >> 16);
            bytes[7] = (byte) (lon >> 24);

            int len = mCodec.encode(bytes, res);
            btConnection.write(res, len);
        }
    }

    private class BtConnection extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private final BluetoothAdapter mBluetoothAdapter;
        private InputStream mmInStream;
        private OutputStream mmOutStream;
        Handler mHandler;
        boolean connected = false;

        BtConnection(BluetoothDevice device, BluetoothAdapter bluetoothAdapter, Handler handler) {
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
            mmDevice = device;
            mBluetoothAdapter = bluetoothAdapter;
            mHandler = handler;
            mmInStream = null;
            mmOutStream = null;

            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                // MY_UUID is the app's UUID string, also used by the server code
                UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard SerialPortService ID
                tmp = device.createRfcommSocketToServiceRecord(uuid);
            } catch (IOException e) {
                Log.e(TAG, "Error creating BT socket!");
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it will slow down the connection
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mmSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and get out
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Error connecting BT device!");
                }
                return;
            }

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                mmInStream = mmSocket.getInputStream();
                mmOutStream = mmSocket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error getting BT streams!");
                return;
            }

            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()

            connected = true;

            mHandler.obtainMessage(STATUS_CHANGE, -1, -1, null).sendToTarget();

            // Keep listening to the InputStream until an exception occurs
            while (true) try {
                // Read from the InputStream
                bytes = mmInStream.read(buffer);
                // Send the obtained bytes to the UI activity
                mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
            } catch (IOException e) {
                break;
            }
            cancel();
        }

        /* Call this from the main activity to send data to the remote device */
        void write(byte[] bytes, int len) {
            try {
                mmOutStream.write(bytes, 0, len);
            } catch (IOException e) {
                Log.e(TAG, "Error writing BT device!");
                cancel();
            }
        }

        /**
         * Will cancel an in-progress connection, and close the socket
         */
        void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Error canceling BT connection!");
            }
            connected = false;
            mHandler.obtainMessage(STATUS_CHANGE, -1, -1, null).sendToTarget();
        }
    }

    private static class MyHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;
        MyHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }
        @Override
        public void handleMessage(Message msg) {
            MainActivity activity = mActivity.get();
            if (activity != null) {
                switch (msg.what) {
                    case MainActivity.MESSAGE_READ: {
                        byte[] buffer = (byte[]) msg.obj;
                        for (int i = 0; i < msg.arg1; i++) {
                            if (activity.mCodec.decode(buffer[i])) {
                                if (activity.mCodec.decodeHeader() == activity.mCodec.positionHeader) {
                                    activity.mPosition.setPosition(activity.mCodec.decodePosition());
                                    activity.mPosition.setVisible(true);
                                }
                                if (activity.mCodec.decodeHeader() == activity.mCodec.relativeCourseHeader) {
                                    activity.relativeCourse = activity.mCodec.decodeRelativeCourse();
                                }
                                activity.setText();
                            }
                        }
                    }
                    case MainActivity.STATUS_CHANGE:
                    {
                        activity.setText();
                    }
                }
            }
        }
    }
}

